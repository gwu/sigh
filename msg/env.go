package msg

import (
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// get environment variable
func (ms *Message) envget() {
	switch {
	case len(ms.argv) < 2:
		ms.throw(4, 0)
		return
	case len(ms.argv) > 2:
		ms.throw(5, 2)
		return
	}

	cnt := ms.dbget("." + ms.argv[1])

	if cnt == "" {
		ms.throw(3, 1)
		return
	}

	ms.s.ChannelMessageSend(ms.m.ChannelID, cnt)
}

// reset guild environment
func (ms *Message) envreset() {
	if ms.m.GuildID != 0 {
		g, _ := ms.s.Guild(ms.m.GuildID)
		if g.OwnerID != ms.m.Author.ID {
			ms.throw(6, 0)
		}
	}

	batch := new(leveldb.Batch)
	itor := ms.db.NewIterator(util.BytesPrefix([]byte(ms.dbkey)), nil)
	if itor.Next() {
		batch.Delete(itor.Key())
	}
	itor.Release()
	ms.db.Write(batch, nil)
}
