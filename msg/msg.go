package msg

import (
	"encoding/csv"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/rumblefrog/discordgo"
	"github.com/spf13/viper"
	"github.com/syndtr/goleveldb/leveldb"
	// "github.com/syndtr/goleveldb/leveldb"
)

type Message struct {
	s      *discordgo.Session
	m      *discordgo.MessageCreate
	db     *leveldb.DB
	dbkey  string
	botcmd map[string]func()
	argv   []string
	optind int
}

func Handle(s *discordgo.Session, m *discordgo.MessageCreate) {
	switch {
	case m.Author.Bot:
		// fmt.Println("bot response")
		return
	case m.Content == "":
		fmt.Println("just an embed")
		return
	}

	ms := Message{s: s, m: m}

	err := ms.handle()
	if err != nil {
		fmt.Println("err: ", err)
		return
	}
}

func (ms *Message) handle() error {
	db, err := leveldb.OpenFile(os.Getenv("HOME")+"/.cache/sigh/", nil)
	if err != nil {
		return err
	}
	defer db.Close()

	ms.db = db

	r := &csv.Reader{}

	if ms.m.GuildID == 0 {
		ms.dbkey = "user." + strconv.FormatInt(ms.m.Author.ID, 10)

		ms.exmap(viper.Get("env"), ms.dbkey+".env")
		r = csv.NewReader(strings.NewReader(ms.m.Content))
	} else {
		ms.dbkey = "guild." + strconv.FormatInt(ms.m.GuildID, 10)
		ms.exmap(viper.Get("env"), ms.dbkey+".env")

		p, err := ms.pcheck()
		if err != nil {
			return err
		}

		r = csv.NewReader(strings.NewReader(strings.TrimSpace(strings.TrimPrefix(ms.m.Content, string(p)))))
	}

	r.Comma = ' '
	ms.argv, err = r.Read()
	if err != nil {
		return err
	}

	ms.cmdinit()

	ms.cmd()

	return err
}

func (ms *Message) pcheck() (p []byte, err error) {
	p, err = ms.db.Get([]byte(ms.dbkey+".env.prefix"), nil)
	if err != nil {
		return
	}

	switch {
	case ms.m.Content == string(p):
		ms.s.ChannelMessageSend(ms.m.ChannelID, "`was i called?`")
		return []byte(""), errors.New("Message only contained server prefix")
	case !strings.HasPrefix(ms.m.Content, string(p)):
		return []byte(""), errors.New("Message doesn't contain prefix")
	}

	return
}
